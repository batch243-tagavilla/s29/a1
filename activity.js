/* 1. */

db.users.find(
    {
        $or: [
            {firstName: "s"},
            {lastName: "d"}
        ]
    },
    {
        firstName: true,
        lastName: true,
        _id: false
    }
).pretty();

/* 2. */

db.users.find(
    {
        $and: [
            {department: "HR"},
            {age: {$gte: 70}}
        ]
    }
)

/* 3. */

db.users.find(
    {
        $and: [
            {firstName: {$regex: "e"}},
            {age: {$lte: 30}}
        ]
    }
)